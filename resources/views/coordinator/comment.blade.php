@extends('layouts.app')

@section('content')
<div class="container">
    <h4>Comment</h4>
    @include('layouts.errors')
    <form action="{{route('coordinator.update',['id'=>$contribution->id])}}" method="post">
        @csrf
        @method('PUT')
        <textarea name="comment" rows="3" class="form-control">{{$contribution->comment}}</textarea>
        <br>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
@endsection
