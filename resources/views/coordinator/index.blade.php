@extends('layouts.app')

@section('content')
<div class="container">
    <p><em>Faculty's Marketing Coordinator must make a comment within 14 days</em></p>
    {{ $contributions->links()}}
    @include('layouts.success')
    <table class="table table-bordered table-hover">
            <caption>List of contributions - {{Auth::user()->faculty->name}}</caption>
            <thead class="thead-light">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Date</th>
                    <th scope="col">Comment Deadline</th>
                    <th scope="col">Student name</th>
                    <th scope="col">File type</th>
                    <th scope="col">Description</th>
                    <th scope="col">Preview</th>
                    <th scope="col">Comment</th>
                    <th scope="col">Status</th>
                </tr>
            </thead>
            <tbody>
                @forelse($contributions as $contribution)
                <tr>
                    <th scope="row">{{$loop->index + 1}}</th>
                    <td>{{$contribution->created_at}}</td>
                    <td>{{\Carbon\Carbon::parse($contribution->created_at)->addWeeks(2)}}</td>
                    <td>{{$contribution->user->name}}</td>
                    <td>
                        @if($contribution->file_extension != 'docx')
                            Image
                        @else
                            Word document
                        @endif
                    </td>
                    <td>
                        @if($contribution->description)
                            {{$contribution->description}}
                        @else
                            <small class="font-italic">no description</small>
                        @endif
                    </td>
                    <td>
                        @if($contribution->file_extension != 'docx')
                            <img width="50" src="{{asset('storage/'.$contribution->file_path)}}" alt="preview">
                        @else
                            <a class="text-secondary" href="{{asset('storage/'.$contribution->file_path)}}"><i class="fas fa-download"></i> Download</a>
                        @endif
                    </td>
                    <td>
                        @if($contribution->comment)
                            <a class="text-secondary" href="{{route('coordinator.comment',['id'=>$contribution->id])}}">{{$contribution->comment}}</a>
                        @else
                            <a class="text-success" href="{{route('coordinator.comment',['id'=>$contribution->id])}}">Add comment</a>
                        @endif
                    </td>
                    <td>
                        @if($contribution->status == 0)
                            <a href="{{route('coordinator.request',['id'=>$contribution->id])}}">select</a>
                        @elseif($contribution->status == 1)
                            <em>waiting for acception</em>
                        @else
                            <strong class="d-block">Selected</strong>
                            <a href="{{route('coordinator.cancel',['id'=>$contribution->id])}}">cancel</a>
                        @endif
                    </td>
                </tr>
                @empty
                    <td colspan="9" class="text-center">No records</td>
                @endforelse
            </tbody>
        </table>
</div>
@endsection
