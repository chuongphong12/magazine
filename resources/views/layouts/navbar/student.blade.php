@if (request()->is('student/create') ? 'active':'')
<li class="nav-item {{request()->is('student/create') ? 'active':''}}">
    <button type="button"
        data-toggle="modal"
        data-target="#exampleModalScrollable"
        class="nav-link btn btn-outline-success" hidden>
          <i class="fab fa-telegram-plane"></i> Contribute</i>
    </button>
</li>
@else
<li class="nav-item {{request()->is('student/create') ? 'active':''}}">
    <button type="button"
        data-toggle="modal"
        data-target="#exampleModalScrollable"
        class="nav-link btn btn-outline-success">
          <i class="fab fa-telegram-plane"></i> Contribute</i>
    </button>
</li>
@endif

<li class="nav-item {{request()->is('student') ? 'active':''}}">
    <a id="contri" class="nav-link" href="{{route('student.index')}}"><i class="fas fa-archive"></i> Contributions</a>
</li>

 <!-- Modal -->
 <div
   class="modal fade"
   id="exampleModalScrollable"
   tabindex="-1"
   role="dialog"
   aria-labelledby="exampleModalScrollableTitle"
   aria-hidden="true"
 >
   <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
     <div class="modal-content">
       <div class="modal-header">
         <h5 class="modal-title" id="exampleModalScrollableTitle">
           Terms and Conditions
         </h5>
         <button
           type="button"
           class="close"
           data-dismiss="modal"
           aria-label="Close"
         >
           <span aria-hidden="true">&times;</span>
         </button>
       </div>
       <div class="modal-body">
         Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean
         commodo ligula eget dolor. Aenean massa. Cum sociis natoque
         penatibus et magnis dis parturient montes, nascetur ridiculus mus.
         Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.
         Nulla consequat massa quis enim. Donec pede justo, fringilla vel,
         aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut,
         imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede
         mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum
         semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula,
         porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem
         ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra
         nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet.
         Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies
         nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget
         condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem
         neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar,
         hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus.
         Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante.
         Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed
         fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed
         consequat, leo eget bibendum sodales, augue velit cursus nunc,
         Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean
         commodo ligula eget dolor. Aenean massa. Cum sociis natoque
         penatibus et magnis dis parturient montes, nascetur ridiculus mus.
         Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.
         Nulla consequat massa quis enim. Donec pede justo, fringilla vel,
         aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut,
         imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede
         mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum
         semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula,
         porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem
         ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra
         nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet.
         Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies
         nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget
         condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem
         neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar,
         hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus.
         Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante.
         Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed
         fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed
         consequat, leo eget bibendum sodales, augue velit cursus nunc,
         Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean
         commodo ligula eget dolor. Aenean massa. Cum sociis natoque
         penatibus et magnis dis parturient montes, nascetur ridiculus mus.
         Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.
         Nulla consequat massa quis enim. Donec pede justo, fringilla vel,
         aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut,
         imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede
         mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum
         semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula,
         porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem
         ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra
         nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet.
         Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies
         nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget
         condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem
         neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar,
         hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus.
         Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante.
         Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed
         fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed
         consequat, leo eget bibendum sodales, augue velit cursus nunc,
       </div>
       <div class="modal-footer">
         <button
           type="button"
           class="btn btn-secondary"
           data-dismiss="modal"
         >
           Disagree
         </button>
         <button id="agrbtn" type="button" class="btn btn-primary">Agree</button>
       </div>
     </div>
   </div>
 </div>
<script>
    window.onload = function () {
        document.getElementById("agrbtn").addEventListener("click", function(){
            window.location.href = "http://127.0.0.1:8000/student/create"
        });
    }
</script>
