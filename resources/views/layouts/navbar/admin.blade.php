<li class="nav-item dropdown">
    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
        <i class="fas fa-cogs"></i> Operations <span class="caret"></span>
    </a>
    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
        <a class="dropdown-item" href="{{route('admin.closure.index')}}">
            <i class="fas fa-wrench"></i> Manage closure dates
        </a>
    </div>
</li>
<li class="nav-item">
    <a class="nav-link" href="{{route('admin.index')}}">Dashboard</a>
</li>