@extends('layouts.app')

@section('content')

<div class="container">
    <p><em>Closure date: {{$closure_instance->closure_date}} | Final closure date: {{$closure_instance->final_closure_date}}</em></p>
    @include('layouts.success')
    @include('layouts.errors')
    <small>{{Auth::user()->faculty->name}}</small>
    {{ $contributions->links()}}
    <table class="table table-bordered table-hover">
        <caption>List of contributions</caption>
        <thead class="thead-light">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Date</th>
                <th scope="col">File name</th>
                <th scope="col">File type</th>
                <th scope="col">Description</th>
                <th scope="col">Preview</th>
                <th scope="col">Comment</th>
                <th scope="col">Funtions</th>
                <th scope="col">Status</th>
            </tr>
        </thead>
        <tbody>
            @forelse($contributions as $contribution)
            <tr>
                <th scope="row">{{$loop->index + 1}}</th>
                <td>{{$contribution->created_at}}</td>
                <td>{{$contribution->file_name}}</td>
                <td>
                    @if($contribution->file_extension != 'docx')
                        Image
                    @else
                        Word document
                    @endif
                </td>
                <td>
                    @if($contribution->description)
                        {{$contribution->description}}
                    @else
                        <a href="{{route('student.edit',['id'=>$contribution->id])}}">Add</a>
                    @endif
                </td>
                <td>
                    @if($contribution->file_extension != 'docx')
                        <img width="50" src="{{asset('storage/'.$contribution->file_path)}}" alt="preview">
                    @else
                        <a href="{{asset('storage/'.$contribution->file_path)}}"><i class="fas fa-download"></i> Download</a>
                    @endif
                </td>
                <td>
                    @if($contribution->comment)
                        {{$contribution->comment}}
                    @else
                        <small class="font-weight-light font-italic">not comment yet</small>
                    @endif
                </td>
                <td>
                    <a class="btn btn-outline-danger btn-sm" href="#"
                        onclick="event.preventDefault();
                        document.getElementById('{{'delete-form-'.$contribution->id}}').submit();">
                        <i class="far fa-trash-alt"></i>
                    </a>
                    <form id="{{'delete-form-'.$contribution->id}}" action="{{route('student.destroy',['id'=>$contribution->id])}}" method="POST" style="display: none;">
                        @csrf
                        @method('DELETE')
                    </form>
                    @if($contribution->description)
                        <a class="btn btn-outline-info btn-sm" href="{{route('student.edit',['id'=>$contribution->id])}}"><i class="far fa-edit"></i></a>
                    @endif
                </td>
                <td>
                    @if($contribution->status == 0)
                        <em>waiting for selection</em>
                    @elseif($contribution->status == 1)
                        <a href="{{route('student.accept',['id'=>$contribution->id])}}">Accept request</a>
                    @else
                        <strong id="select">Selected</strong>
                    @endif
                </td>
            </tr>
            @empty
                <td colspan="9" class="text-center">No records</td>
            @endforelse
        </tbody>
    </table>

</div>
@endsection
