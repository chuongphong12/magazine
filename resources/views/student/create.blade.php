@extends('layouts.app')

@section('content')
<div class="container">
    @include('layouts.success')
    @include('layouts.errors')
    <small>Upload your contributions here</small>
    <form class="dropzone" action="{{route('student.store')}}" method="post" enctype="multipart/form-data" id="my-awesome-dropzone" onclick="showUpload()">
        @csrf
    </form>

    <div>
        <a id="upload" class="btn btn-warning" style="width: 100%; margin-top: 20px;" role="button" >Upload</a>
        <a id="finish" class="btn btn-primary" href="{{ route('student.noti') }}" style="width: 100%; margin-top: 20px;" role="button" hidden="true">Finish</a>
    </div>

    <script type="text/javascript">
        Dropzone.options.myAwesomeDropzone  =
        {
            // autoProcessQueue: false,
            // maxFilesize: 5,
            // acceptedFiles: ".jpeg,.png,.docx",
            // timeout: 5000,
            // success: function(file, response)
            // {
            //     console.log(response.success);
            // },
            // error: function(file, response)
            // {
            //    return false;
            // }

            autoProcessQueue: false,
            maxFilesize: 5,
            acceptedFiles: ".jpg,.jpeg,.png,.docx,.doc",
            timeout: 5000,

            // The setting up of the dropzone
            init: function() {
                var a = document.getElementById("upload")
                var submitButton = document.querySelector("#upload")
                var myDropzone = this;

            // First change the button to actually tell Dropzone to process the queue.
            submitButton.addEventListener("click", function(e) {
                if(myDropzone.files.length > 0) {
                    e.preventDefault();
                    e.stopPropagation();
                    myDropzone.processQueue();
                    a.setAttribute("hidden","true");
                    document.getElementById("finish").hidden = false;
                    document.getElementById("contri").hidden = true;
                } else {
                    alert("Upload file is empty!!")
                    return false;
                }
            // Make sure that the form isn't actually being sent.

            });

            // Listen to the sendingmultiple event. In this case, it's the sendingmultiple event instead
            // of the sending event because uploadMultiple is set to true.
            this.on("sendingmultiple", function() {
            // Gets triggered when the form is actually being sent.
            // Hide the success button or the complete form.
            });
            this.on("success", function(files, response) {
                console.log(response.success);
            });
            this.on("error", function(files, response) {
                return false;
            });
            }
    };

    // function showFinish() {
    //     document.getElementById("finish").hidden = false;
    // }

    // function showUpload() {
    //     setTimeout(function(){
    //         document.getElementById("upload").hidden = false;
    //     }, 3000);
    // }
    // window.onbeforeunload = function() { return "Your work will be lost."; };
    history.pushState(null, null, location.href);
    window.onpopstate = function () {
        history.go(1);
    };
    </script>
</div>
@endsection
