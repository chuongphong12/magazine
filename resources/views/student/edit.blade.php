@extends('layouts.app')

@section('content')
<div class="container">
    <h4>Description</h4>
    <form action="{{route('student.update',['id'=>$contribution->id])}}" method="post">
        @csrf
        @method('PUT')
        <textarea name="description" rows="3" class="form-control">{{$contribution->description}}</textarea>
        <br>
        <button type="submit" class="btn btn-primary">Update</button>
    </form>
</div>
@endsection
