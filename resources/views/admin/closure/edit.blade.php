@extends('layouts.app')

@section('content')
<div class="container">
    <h4>Update closure dates</h4>
    @include('layouts.success')
    @include('layouts.errors')
    <div class="card w-50">
        <div class="card-body">
            <form action="{{route('admin.closure.update')}}" method="post">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label>Closure Date</label>
                    <input type="date" name="closure_date" class="form-control" value="{{$closure->closure_date}}">
                </div>
                <div class="form-group">
                    <label>Final Closure Date</label>
                    <input type="date" name="final_closure_date" class="form-control" value="{{$closure->final_closure_date}}">
                </div>
                <button class="btn btn-primary" type="submit">Update</button>
            </form>
        </div>
    </div>
</div>
@endsection
