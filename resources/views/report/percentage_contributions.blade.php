@extends('layouts.app')

@section('content')
<div class="container">
    <form action="{{route('report.update.percentage')}}" method="post">
        @csrf
        <select class="form-control d-inline-block" style="width:100px" onchange="this.form.submit()" name="year">
            <option value="9999">All time</option>
            <option value="2019" {{$current==2019?'selected':''}}>2019</option>
            <option value="2018" {{$current==2018?'selected':''}}>2018</option>
            <option value="2017" {{$current==2017?'selected':''}}>2017</option>
            <option value="2016" {{$current==2016?'selected':''}}>2016</option>
            <option value="2015" {{$current==2015?'selected':''}}>2015</option>
        </select>
    </form>
    <div class="row">
        <div class="col-sm-3"></div>
        <div class="col-sm-6">
            <div class="chart-container">
                <canvas id="myChart" width="400" height="400"></canvas>
            </div>
        </div>
        <div class="col-sm-3"></div>
    </div>
    <br>
    <table class="table table-bordered">
        <caption>Percentages of contributions</caption>
        <thead class="thead-light">
            <tr>
                <th>#</th>
                <th>Faculty</th>
                <th>Percentage</th>
            </tr>
        </thead>
        <tbody>
           @foreach($percentages as $key => $value)
            <tr>
                <td>{{$loop->index + 1}}</td>
                <td>{{$key}}</td>
                <td>{{$value }} %</td>
            </tr>
           @endforeach
        </tbody>
    </table>
    <script>
        var chartColors = {
          red: 'rgb(255, 99, 132)',
          orange: 'rgb(255, 159, 64)',
          yellow: 'rgb(255, 205, 86)',
          green: 'rgb(75, 192, 192)',
          blue: 'rgb(54, 162, 235)',
          purple: 'rgb(153, 102, 255)',
          grey: 'rgb(201, 203, 207)'
        };
        var config = {
          type: 'pie',
          data: {
            datasets: [{
              data: {{ 
                $data->map(function($item, $key){
                    return $item;
                })
              }},
              backgroundColor: [
                  chartColors.blue,
                  chartColors.red,
                  chartColors.yellow,
                  chartColors.green,
                  chartColors.purple,
                  chartColors.grey,
                  chartColors.orange
              ]
            }],
            labels: {!!
                $labels->map(function($item, $key){
                    return $item;
                })
            !!}
          },
          options: {
              responsive:true
          }
        };
        window.onload = function() {
          var ctx = document.getElementById('myChart').getContext('2d');
          var myChart = new Chart(ctx, config);
        };
      </script>
</div>
@endsection
