<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Auth::routes(['register'=>false]);
Route::get('/home', 'HomeController@index')->name('home');

// Student
Route::get('/student','StudentController@index')->name('student.index');
Route::get('/student/create', 'StudentController@create')->name('student.create')->middleware('check.closure.date');
Route::post('/student', 'StudentController@store')->name('student.store')->middleware('check.closure.date');
Route::get('/student/{id}/edit','StudentController@edit')->name('student.edit')->middleware('check.final.closure.date');
Route::put('/student/{id}','StudentController@update')->name('student.update')->middleware('check.final.closure.date');
Route::delete('/student/{id}','StudentController@destroy')->name('student.destroy')->middleware('check.final.closure.date');
Route::get('/student/{id}/accept', 'StudentController@accept')->name('student.accept')->middleware('check.final.closure.date');
Route::get('/send-noti', 'StudentController@sendNoti')->name('student.noti');

// Marketing Coordinator
Route::get('/coordinator', 'CoordinatorController@index')->name('coordinator.index');
Route::get('/coordinator/{id}/comment', 'CoordinatorController@comment')->name('coordinator.comment')->middleware('check.final.closure.date');
Route::get('/coordinator/{id}/request','CoordinatorController@request')->name('coordinator.request')->middleware('check.final.closure.date');
Route::get('/coordinator/{id}/cancel', 'CoordinatorController@cancel')->name('coordinator.cancel')->middleware('check.final.closure.date');
Route::put('/coordinator/{id}', 'CoordinatorController@update')->name('coordinator.update')->middleware('check.final.closure.date');

// Guest
Route::get('/guest', 'GuestController@index')->name('guest.index');

// Marketing Manager
Route::get('/manager', 'ManagerController@index')->name('manager.index');
Route::get('/manager/download','ManagerController@download')->name('manager.download');

// Administrator
Route::get('/admin', 'AdminController@index')->name('admin.index');

Route::get('/admin/closure/index', 'AdminController@indexClosure')->name('admin.closure.index');
Route::get('/admin/closure/{id}/edit', 'AdminController@editClosure')->name('admin.closure.edit');
Route::get('/admin/closure/create', 'AdminController@createClosure')->name('admin.closure.create');
Route::put('/admin','AdminController@updateClosure')->name('admin.closure.update');
Route::post('/admin/closure','AdminController@storeClosure')->name('admin.closure.store');

// Reports

Route::get('/report/contributions', 'ReportController@contributions')->name('report.contributions');
Route::get('/report/contributors', 'ReportController@contributors')->name('report.contributors');
Route::get('/report/percentage', 'ReportController@percentage')->name('report.percentage');
Route::get('/report/withoutcomment', 'ReportController@withoutConment')->name('report.withoutConment');
Route::get('/report/afterfourteen', 'ReportController@afterFourteen')->name('report.afterFourteen');

Route::post('/report/percentage', 'ReportController@updatePercentage')->name('report.update.percentage');