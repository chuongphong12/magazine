<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Contribution;

class Faculty extends Model
{
    public function contributions()
    {
        return $this->hasManyThrough('App\Contribution','App\User');
    }

    public function percentage(){
        $count = $this->contributions->count();
        $total = Contribution::all()->count();
        return round($count/$total*100);
    }
    public function percentageInYear($year){
        $total = Contribution::whereYear('created_at',$year)->get()->count();
        $count = Contribution::where('faculty_id',$this->id)->whereYear('created_at',$year)->get()->count();
        if($total==0){
            return 0;
        } else {
            return round($count / $total * 100);
        }
    }
    public function contributionsInYear($year){
        $contributions = Contribution::whereYear('created_at',$year)->get()->count();
        return $contributions;
    }

    public function countContributionsInASpecificYear($year){
        $count = Contribution::where('faculty_id',$this->id)->whereYear('created_at',$year)->get()->count();
        return $count;
    }
}
