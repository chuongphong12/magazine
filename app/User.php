<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function faculty(){
        return $this->belongsTo('App\Faculty');
    }
    public function isAdmin(){
        return $this->user_type == 0;
    }
    public function isMarketingManager(){
        return $this->user_type == 1;
    }
    public function isMarketingCoordinator(){
        return $this->user_type == 2;
    }
    public function isStudent(){
        return $this->user_type == 3;
    }
    public function isGuest(){
        return $this->user_type == 4;
    }
}
