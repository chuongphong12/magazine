<?php

namespace App\Http\Middleware;

use Closure;
use Carbon\Carbon;

class CheckClosureDate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $datenow = Carbon::now('Asia/Ho_Chi_Minh');
        $closure = \App\Closure::where('academic_year',$datenow->year)->first(); //take the closure record of the current year
        $closure_date = Carbon::parse($closure->closure_date);  //parse the closure date into Carbon instance
        if($datenow->greaterThan($closure_date)) {
            return redirect()->route('student.index')->withErrors(["We appreciate your contribution, but we can't receive new contribution after the closure date! "]);
        }
        return $next($request);
    }
}
