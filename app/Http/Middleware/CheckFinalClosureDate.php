<?php

namespace App\Http\Middleware;

use Closure;
use Carbon\Carbon;

class CheckFinalClosureDate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $datenow = Carbon::now('Asia/Ho_Chi_Minh');
        $closure = \App\Closure::where('academic_year',$datenow->year)->first(); //take the closure record of the current year
        $final_closure_date = Carbon::parse($closure->final_closure_date);  //parse the closure date into Carbon instance
        if($datenow->greaterThan($final_closure_date)) {
            return redirect('/')->withErrors(["We just passed the Final Closure Date, no operations are allowed now!","See you next year !!"]);
        }
        return $next($request);
    }
}
