<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contribution;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class ManagerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function($request, $next){
            abort_unless(Auth::user() -> isMarketingManager(), 403);
            return $next($request);
        });
    }
    public function index(){
        $contributions = Contribution::where('status',2)->orderBy('id','DESC')->paginate(10);
        return view('manager.index',compact('contributions'));
    }
    public function download(){
        $datenow = Carbon::now('Asia/Ho_Chi_Minh');
        $closure = \App\Closure::where('academic_year',$datenow->year)->first(); //take the closure record of the current year
        $final_closure_date = Carbon::parse($closure->final_closure_date);  //parse the closure date into Carbon instance
        if($datenow->lessThan($final_closure_date)) {
            return redirect()->route('manager.index')->withErrors(["Download is only available after the final closure date"]);
        }

        Storage::delete('public/contributions.zip');
        $contributions = Contribution::where('status',2)->get();
        $selected = $contributions->map(function($item, $key){
            return public_path('storage/').$item->file_path;
        });
        $files = $selected->all();
        $zip = \Zipper::make(public_path('storage/contributions.zip'));
        $zip->add($files)->close();
        return Storage::download('public/contributions.zip');
    }
}
