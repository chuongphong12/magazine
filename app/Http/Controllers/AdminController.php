<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Closure;
use Carbon\Carbon;
use App\Contribution;

class AdminController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
        $this->middleware(function($request, $next){
            abort_unless(Auth::user() -> isAdmin(), 403);
            return $next($request);
        });
    }
    public function index(){
        $contributions = Contribution::paginate(10);
        return view('admin.index',compact('contributions'));
    }
    public function indexClosure(){
        $closures = Closure::all();
        return view('admin.closure.index', compact('closures'));
    }
    public function createClosure(){
        return view('admin.closure.create');
    }
    public function storeClosure(Request $request){
        $request->validate([
            'closure_date'=>'required',
            'final_closure_date'=>'required'
        ]);
        if($request->closure_date > $request->final_closure_date){
            return back()->withErrors(['Final closure date must be greater than the closure date']);
        }
        $dt = Carbon::parse($request->closure_date);
        try {
            $closure = new Closure;
            $closure->closure_date = $request->closure_date;
            $closure->final_closure_date = $request->final_closure_date;
            $closure->academic_year = $dt->year;
            $closure->save();
            $request->session()->flash('status', 'Done!');
            return redirect()->route('admin.closure.index');
        } catch(\Exception $e){
            return back()->withErrors(['This year has been created!']);
        }
        
    }
    public function editClosure($id){
        $closure = Closure::findOrFail($id);
        return view('admin.closure.edit', compact('closure'));
    }
    public function updateClosure(Request $request){
        $request->validate([
            'closure_date'=>'required',
            'final_closure_date'=>'required'
        ]);
        if($request->closure_date > $request->final_closure_date){
            return back()->withErrors(['Final closure date must be greater than the closure date']);
        }
        $closure = Closure::first();
        $closure->closure_date = $request->closure_date;
        $closure->final_closure_date = $request->final_closure_date;
        $closure->save();
        $request->session()->flash('status', 'Done!');
        return redirect()->route('admin.closure.index');
    }
}
